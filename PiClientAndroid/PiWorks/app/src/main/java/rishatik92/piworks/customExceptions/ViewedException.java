package rishatik92.piworks.customExceptions;

/**
 * Created by Askar on 18.09.2017.
 */

public class ViewedException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // Default constructor
    // initializes custom exception variable to none
    public ViewedException() {
        // call superclass constructor
        super();
    }

    // Custom Exception Constructor
    public ViewedException(String message) {
        // Call super class constructor
        super(message);
    }


}