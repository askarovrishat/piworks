package rishatik92.piworks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import rishatik92.piworks.customExceptions.AlertUser;
import rishatik92.piworks.customExceptions.ViewedException;

/**
 * Created by Rishat Askarov on 17.09.2017.
 */

public class TcpClient {

    private Socket mySocket;
    private BufferedReader fromServer;
    private PrintWriter toServer;

    public TcpClient(String ipAddress, int port, MainActivity activity) {
        try {
            mySocket = new Socket(ipAddress, port);
        } catch (IOException e) {
            AlertUser.alertUser(activity, "Не могу подключиться, проверьте доступность...");
        }
        try {
            toServer = new PrintWriter(mySocket.getOutputStream());
            fromServer = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
        } catch (IOException e) {
            AlertUser.alertUser(activity, "Buffer creation problem..");
        }
    }

    public void println(String message) {
        toServer.print(message + "\n");
        toServer.flush();
    }

    public String readLine() {

        try {
            return fromServer.readLine();
        } catch (IOException e) {
            throw new ViewedException("Проблемы с чтением сообщений от сервера.");
        }
    }

    public boolean ready() {
        try {
            return fromServer.ready();
        } catch (IOException e) {
            throw new ViewedException("Проблемы с получением состояния буфера чтения.");
        }

    }

}
