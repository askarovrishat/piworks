package rishatik92.piworks.customExceptions;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by Askar on 18.09.2017.
 */

public class AlertUser {

    public static void alertUser(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Внимание!");
        dialog.setMessage(message);
        dialog.setNeutralButton("Ok", null);
        dialog.create().show();
    }
}
