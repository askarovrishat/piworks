package rishatik92.piworks;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
    private TcpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // нужно чтобы получить разрешение на подключение..
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);
        final EditText ipAddressView = (EditText) findViewById(R.id.ipAddress);
        final EditText portView = (EditText) findViewById(R.id.port);
        final TextView connectionStatusView = (TextView) findViewById(R.id.connectionStatus);
        final TextView connectionLogView = (TextView) findViewById(R.id.currentLog);
        final ToggleButton connectButtonView = (ToggleButton) findViewById(R.id.connecting);
        final Button turnOff = (Button) findViewById(R.id.disablePWM);
        final SeekBar setPwmVolume = (SeekBar) findViewById(R.id.setVolumePwm);
        final SeekBar timeBlinker = (SeekBar) findViewById(R.id.pwmTimeIndicator);
        final Button toBlink = (Button) findViewById(R.id.blinker);
        turnOff.setEnabled(false);
        toBlink.setEnabled(false);
        connectButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connectButtonView.isActivated()) {

                    client = new TcpClient(ipAddressView.getText().toString(), Integer.parseInt(portView.getText().toString()), MainActivity.this);
                    connectionStatusView.setText("trying to connect..");
                    connectionLogView.clearComposingText();

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    while (client.ready()) {
                        client.readLine();
                    }
                    String reply = "";
                    client.println("s");
                    while (true) {
                        reply = client.readLine();
                        if (reply.equals("{\"success\":true,\"message\":\"ready to work\"}")) break;
                    }
                    connectionLogView.append(reply);
                    connectionStatusView.setText("Conected!");


                    connectButtonView.setActivated(true);
                    turnOff.setEnabled(true);
                    toBlink.setEnabled(true);
                } else {
                    client.println("{\"logout\":true}");
                    turnOff.setEnabled(false);
                    toBlink.setEnabled(false);
                    connectButtonView.setActivated(false);
                }
            }
        });
        turnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                client.println("{\"set\":{\"pwm\":{\"type\":\"turnOff\"}}}");
                turnOff.setActivated(false);
            }

        });
        setPwmVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                client.println("{\"set\":{\"pwm\":{\"type\":\"set\",\"volume\":" + progress + "}}}");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        toBlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.println(" {\"set\":{\"pwm\":{\"type\":\"blink\",\"time\":" + (100 - timeBlinker.getProgress()) + "}}}");

            }
        });

        timeBlinker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                client.println(" {\"set\":{\"pwm\":{\"type\":\"blink\",\"time\":" + (100 - progress) + "}}}");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
}
