package rishatik92.PiService;

/**
 * Перечисление видов работ которые может выполнять Pwm сервис.
 * 
 * @author rishat
 *
 */
public enum PwmKindOfWork {

	NOTHING, BLINKING, ASSIGNED;
}
