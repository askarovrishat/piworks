package rishatik92.PiService;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;


/**
 * Интерфейс описывающий функции малинки.
 * 
 * @author Askarov Rishat
 *
 */
public interface PiService extends Remote {
	
	/**
	 * установить PWM пины
	 * @param pins - массив
	 * @return - возвращает успешно установленные пины
	 * @throws RemoteException
	 */
	public ArrayList<Integer> setPinForPwm(ArrayList<Integer> pins) throws RemoteException;
	
	/**
	 *  Выбрать режим работы PWM
	 * @param pwmKindOfWork
	 * @throws RemoteException
	 */
	public void setWorkKind(PwmKindOfWork pwmKindOfWork)  throws RemoteException;
	
	/**
	 *  вернуть время между сменой уровня PWM
	 * @return
	 * @throws RemoteException
	 */
	public int getTimeForBlinking() throws RemoteException;
	/**
	 * Вернуть заданный сейчас тип работы PWM
	 * @return
	 * @throws RemoteException
	 */
	public PwmKindOfWork getWorkKind() throws RemoteException;
	/**
	 * Установить время между сменой уровня PWM
	 * @param time
	 * @throws RemoteException
	 */
	public void setTimeForBlinking(int time) throws RemoteException;
	
	/**
	 * Установить уровень PWM
	 * @param volume
	 * @throws RemoteException
	 */
	public void setVolume(int volume) throws RemoteException;
}