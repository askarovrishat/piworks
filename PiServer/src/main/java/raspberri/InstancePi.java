package raspberri;


import java.util.LinkedHashSet;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;


/**
 * Данный класс реализует управление пинами GPIO raspberryPi.
 * Может существовать в единственном экземпляре.
 * - реализованна поддержка Hardware PWM - пины номер 1, 23, 24, 26 (нумерация wiringPi)
 * @author rishat
 *
 */
public class InstancePi {
	
	private static InstancePi instance;
	private GpioController gpio;
	private LinkedHashSet<GpioPinPwmOutput> pwmPins = new LinkedHashSet<GpioPinPwmOutput>();
	private LinkedHashSet<Pin> usedPins = new LinkedHashSet<Pin>();
	private boolean pwmEnabled = false;
	private InstancePi() {

		// Здесь объявляем пины.
		gpio = GpioFactory.getInstance();
	}
	
	/**
	 * Получить инстанс.
	 * @return
	 */
	public static synchronized InstancePi getInstance() {
		
		if (instance == null) {
			instance = new InstancePi();
		}
		return instance;
	}
	
	private void enablePwm() {
		com.pi4j.wiringpi.Gpio.pwmSetMode(com.pi4j.wiringpi.Gpio.PWM_MODE_MS);
		com.pi4j.wiringpi.Gpio.pwmSetRange(100);
		com.pi4j.wiringpi.Gpio.pwmSetClock(100);
		System.out.println("PWM enabled");
		pwmEnabled = true;
	}
	/**
	 * Функция добавляющая номер пина в управляемый набор PWM
	 * @param pin -номер пина.
	 * @return - результат добавления.
	 */
	public boolean addPwmPin(int pin) {

		switch (pin) {
		case 1:
			return addPwmPin(RaspiPin.GPIO_01);
		case 23:
			return addPwmPin(RaspiPin.GPIO_23);
		case 24:
			return addPwmPin(RaspiPin.GPIO_24);
		case 26:
			return addPwmPin(RaspiPin.GPIO_26);
		}
		return false;
	}
	private boolean addPwmPin(Pin pin) {
		usedPins.add(pin);
		return pwmPins.add(gpio.provisionPwmOutputPin(pin));
	}
	
	
	/**
	 * Функция устанавливающая уровень PWM (от 0 до 100)
	 * @param level - уровень от 0 до 100
	 * @return результат установки.
	 */
	public boolean setAllPwmLevel(int level) {
		boolean success = false;
		
		if (!pwmEnabled) {
			enablePwm();
		}
		
		for (GpioPinPwmOutput pin: pwmPins) {
			pin.setPwm(level);
			success = (pin.getPwm() == level);
		}
		return success;
	}
	
	/**
	 * Выключить все GPIO если служба не запущена.
	 */
	protected void finalize() {
		System.out.println("disable gpio");
		gpio.shutdown();	
	}
	
}
