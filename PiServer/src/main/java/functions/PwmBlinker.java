package functions;

import raspberri.InstancePi;

/**
 * Класс реализовывает интерфейс работы с ШИМ. - автоматическое мигание - ручная
 * задача яркости
 * 
 * @author rishat
 *
 */
public class PwmBlinker implements Runnable {

	private static PwmBlinker instance;
	private boolean working = false;
	private static InstancePi pi = InstancePi.getInstance();
	// время между сменой значения, для режима мигания.
	private int timeForBlinking = 50;
	// максимальное значение для PWM по умолчанию
	private int max = 100;

	/**
	 * Вернуть состояние, работает он в данный момент или нет.
	 * 
	 * @return
	 */
	public boolean isAlive() {
		return working;
	}

	/**
	 * Остановить выполнение.
	 */
	public void stop() {
		working = false;
	}

	/**
	 * Запустить выполнение потока.
	 */
	public void start() {
		working = true;
	}

	/**
	 * Установить время смены значений для мигания
	 * 
	 * @param time
	 */
	public void setTimeForBlinking(int time) {
		timeForBlinking = time;
	}

	/**
	 * Вернуть время мигания.
	 * 
	 * @return
	 */
	public int getTimeForBlinking() {
		return timeForBlinking;
	}

	/**
	 * Данный тип работ может выполняться только одним владельцем. Поэтому
	 * должен существовать только один объект runnable.
	 * 
	 * @return instance
	 */
	public static synchronized PwmBlinker getInstance() {

		if (instance == null) {
			instance = new PwmBlinker();
		}
		return instance;
	}

	/**
	 * Сам процесс мигания
	 */
	public void run() {
		while (true) {
			if  (isAlive()) {
				for (int i = 0; (i <= max) && isAlive(); i += 1) {
					setPwmAndSleep(i, timeForBlinking);
				}
				for (int i = max; (i >= 0) && isAlive(); i -= 1) {
					setPwmAndSleep(i, timeForBlinking);
				}
			} else {
				// Если ничего не делать - спать.
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}

	}

	private static void setPwmAndSleep(int value, int milliSecToSleep) {
		if (milliSecToSleep <= 0) {
			milliSecToSleep = 1;
		}
		pi.setAllPwmLevel(value);
		try {
			Thread.sleep(milliSecToSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
