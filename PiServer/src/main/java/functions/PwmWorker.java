package functions;

import java.util.ArrayList;

import raspberri.InstancePi;
import functions.PwmBlinker;
import rishatik92.PiService.PwmKindOfWork;

public class PwmWorker implements rishatik92.PiService.PiService {
	private static InstancePi pi = InstancePi.getInstance();
	private PwmKindOfWork statusWorking = PwmKindOfWork.NOTHING;
	private PwmBlinker blinker = PwmBlinker.getInstance();
	private int level = 100;
	private Thread BlinkThread = null;

	/**
	 * Добавление PWM пинов в управляемый набор.
	 * 
	 * @param pins
	 *            - массив с пинами
	 * @return - возвращаем массив с успешно добавленными пинами.
	 */
	public ArrayList<Integer> setPinForPwm(ArrayList<Integer>  pins) {
		ArrayList<Integer> addedPins = new ArrayList<Integer>();
		for (int pin : pins) {
			if (pi.addPwmPin(pin)) {
				addedPins.add(Integer.valueOf(pin));
			}
		}


		return addedPins;
	}

	/**
	 * Установить тип работ.
	 * 
	 * @param kindOfWork
	 */
	public void setWorkKind(PwmKindOfWork pwmKindOfWork) {
		statusWorking = pwmKindOfWork;
		switch (pwmKindOfWork) {
		case ASSIGNED:
			stopBlink();
			pi.setAllPwmLevel(level);
			break;
		case BLINKING:
			startBlink();
			break;
		case NOTHING:
			stopBlink();
			pi.setAllPwmLevel(0);
		}
	}

	/**
	 * Вернуть тип заданной сейчас работы
	 * 
	 * @return statusWorking
	 */
	public PwmKindOfWork getWorkKind() {
		return statusWorking;
	}

	/**
	 * Установить время смены значений для мигания
	 * 
	 * @param time
	 */
	public void setTimeForBlinking(int time) {
		blinker.setTimeForBlinking(time);
	}

	/**
	 * Установить яркость.
	 * 
	 * @param volume
	 */
	public void setVolume(int volume) {
		level = volume;
	}

	/**
	 * Вернуть время мигания.
	 * 
	 * @return
	 */
	public int getTimeForBlinking() {
		return blinker.getTimeForBlinking();
	}

	private void startBlink() {
		if (!blinker.isAlive()) {
			blinker.start();
		}

		if (BlinkThread == null) {
			BlinkThread = new Thread(blinker);
			BlinkThread.start();
		}

	}

	private void stopBlink() {
		if ((BlinkThread != null) && (blinker.isAlive())) {
			blinker.stop();
		}
	}
}
