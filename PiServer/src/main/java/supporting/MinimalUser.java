package supporting;

import org.json.JSONObject;

import functions.PwmWorker;

public class MinimalUser extends RaspberryUser {

	public MinimalUser(String name, InputOutput channel) {
		super(name, channel);
	}

	public boolean setPwmtoBlink(int timeForBlinking) {
		myChannel.setDebug("Not permitted for user.");
		return false;

	}

	public boolean setVolumePWM(int volume) {
		myChannel.setDebug("Not permitted for user.");
		return false;

	}

	public boolean turnOff() {
		myChannel.setDebug("Not permitted for user.");
		return false;
	}

	public InputOutput getChannel() {
		return myChannel;
	}

}
