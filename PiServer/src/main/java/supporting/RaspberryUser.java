package supporting;

import org.json.JSONObject;

import functions.PwmWorker;

public abstract class RaspberryUser {

	protected String myName;
	protected PwmWorker myPwm;
	protected InputOutput myChannel;

	public RaspberryUser(String name, InputOutput channel) {
		name = myName;
		myChannel = channel;
	}

	public boolean setName(String name) {
		if (name != null) {
			myName = name;
			return true;
		} else {
			return false;
		}
	}

	public boolean setPwm(PwmWorker pwm) {
		if (pwm != null) {
			myPwm = pwm;
			return true;
		} else {
			return false;
		}

	}

	public boolean setChannel(InputOutput channel) {
		if (channel != null) {
			myChannel = channel;
			return true;
		} else {
			return false;
		}

	}

	public abstract InputOutput getChannel();

	/*
	 * Установить PWM как мигающий.
	 */
	public abstract boolean setPwmtoBlink(int timeForBlinking);

	/*
	 * Установить яркость PWM
	 */
	public abstract boolean setVolumePWM(int volume);

	/*
	 * Выключить PWM
	 */
	public abstract boolean turnOff();

	/*
	 * Здесь генерируются ответы пользователю на запросы типа "get"
	 */
	public boolean doGet(Object getAction) {
		JSONObject action = (JSONObject) getAction;
		return false;
	}

	/*
	 * Здесь парсятся "set" запросы пользователя.
	 */
	public boolean doSet(Object setAction) {
		JSONObject module = (JSONObject) setAction;
		if (module.keySet().size() != 1) {
			myChannel.setDebug("count of action modules is not equal 1");
			return false;
		} else if (module.has("pwm")) {
			JSONObject pwmAction = (JSONObject) module.get("pwm");
			String typeOfWork = pwmAction.getString("type");
			switch (typeOfWork) {
			case "blink":
				int timeForBlinking = (Integer) pwmAction.get("time");
				return setPwmtoBlink(timeForBlinking);
			case "set":
				int volume = (Integer) pwmAction.get("volume");
				return setVolumePWM(volume);
			case "turnOff":
				return turnOff();
			default:
				myChannel.setDebug("unknown pwmAction");
				return false;
			}
		}
		myChannel.setDebug("unknown module");
		return false;

	}
}
