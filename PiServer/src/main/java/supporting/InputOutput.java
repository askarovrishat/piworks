package supporting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class InputOutput {
	private PrintWriter output;
	private BufferedReader input;
	private StringBuffer debug = new StringBuffer();

	/*
	 * Get input and output buffers for read and write messages.
	 */
	public InputOutput(BufferedReader in, PrintWriter out) {
		input = in;
		output = out;
	}

	public InputOutput() {
		/*
		 * Default using console output
		 */

		this(new BufferedReader(new InputStreamReader(System.in)), new PrintWriter(System.out));
	}

	/*
	 * Get input and output from socket.
	 */
	public InputOutput(Socket socket) throws IOException {
		this(new BufferedReader(new InputStreamReader(socket.getInputStream())),
				new PrintWriter(socket.getOutputStream()));
	}

	public void println(String message) {
		output.print(message + "\n");
		output.flush();
	}

	public String readLine() {
		try {
			return input.readLine();
		} catch (IOException e) {
			println("Unknown response..");
			return "";
		}
	}

	/*
	 * return True if any message in channel out.
	 */
	public boolean ready() throws IOException {
		return input.ready();
	}

	/*
	 * clear all string buffer
	 */
	public void clearDebugInfo() {
		debug.delete(0, debug.length());
	}

	/*
	 * Read all from debug buffer
	 */
	public String readDebug() {
		return debug.toString();
	}

	/*
	 * Add any debug message to debugBuffer
	 */
	public void appendDebug(String debugMesage) {
		debug.append(debugMesage);
	}
	
	public void setDebug(String debugMesage){
		clearDebugInfo();
		appendDebug(debugMesage);
	}

}
