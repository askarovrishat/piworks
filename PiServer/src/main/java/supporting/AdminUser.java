package supporting;

import rishatik92.PiService.PwmKindOfWork;

public class AdminUser extends MinimalUser {

	public AdminUser(String name, InputOutput channel) {
		super(name, channel);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean setPwmtoBlink(int timeForBlinking) {
		if (myPwm != null) {
			myPwm.setTimeForBlinking(timeForBlinking);
			myPwm.setWorkKind(PwmKindOfWork.BLINKING);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean setVolumePWM(int volume) {
		if (myPwm != null) {
			myPwm.setVolume(volume);
			myPwm.setWorkKind(PwmKindOfWork.ASSIGNED);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean turnOff() {
		if (myPwm != null) {
			myPwm.setWorkKind(PwmKindOfWork.NOTHING);
			return true;
		} else {
			return false;
		}

	}

}
