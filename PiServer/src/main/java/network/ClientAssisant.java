package network;

import java.io.IOException;
import java.net.Socket;

import functions.PwmWorker;
import general.InteractiveServer;
import supporting.InputOutput;

public class ClientAssisant implements Runnable {
	private Socket socket;
	private String infoMessage;
	private PwmWorker pwm;
	private InputOutput socketChannel;

	public ClientAssisant(Socket gettedSocket, String gettedInfoMessage, PwmWorker gettedPwm) {
		pwm = gettedPwm;
		infoMessage = gettedInfoMessage;
		socket = gettedSocket;
		try {

			socketChannel = new InputOutput(socket);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		InteractiveServer tcpDialogClient = new InteractiveServer(pwm, socketChannel);
		/*
		 * Здесь программа общается с малинкой через TCP
		 */
		tcpDialogClient.getService(infoMessage);
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
