package network;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import functions.PwmWorker;

public class ConnectionHandler implements Runnable {

	private PwmWorker pwm;
	private String InterfaceName = "tun0";
	private int port = 11818;
	private int maxConnections = 50;
	private String infoMessage = "RaspberryPi tcp Server";

	public ConnectionHandler(PwmWorker workingPwm) {
		pwm = workingPwm;
		infoMessage += "\npwm added.";
	}

	public void run() {
		ServerSocket serverSock;
		try {
			serverSock = new ServerSocket(port, maxConnections, getIpAdress(InterfaceName));

			while (true) {
				Socket clientSocket = serverSock.accept();
				Thread client = new Thread(new ClientAssisant(clientSocket, infoMessage, pwm));
				client.start();
			}
		} catch (IOException e) {
			System.out.println("Can't create network socket.");
			e.printStackTrace();
		}
	}

	/*
	 * Узнать ip адрес на переданном интерфейсе
	 */
	private InetAddress getIpAdress(String interfaceName) throws SocketException {
		NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
		Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
		InetAddress currentAddress;
		do {
			currentAddress = inetAddress.nextElement();
			if (currentAddress instanceof Inet4Address && !currentAddress.isLoopbackAddress()) {
				break;

			}
		} while (inetAddress.hasMoreElements());
		return currentAddress;
	}

}
