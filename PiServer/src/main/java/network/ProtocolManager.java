package network;

import supporting.InputOutput;
import supporting.RaspberryUser;

import org.json.*;

public class ProtocolManager {

	private RaspberryUser myUser;
	protected boolean sessionAlive = true;
	InputOutput userChannel = null;

	public ProtocolManager(RaspberryUser user) {
		myUser = user;
		sendToClient(true, "ready to work");
		userChannel = myUser.getChannel();
		createSession();
	}

	/*
	 * Создать и держать сессию пока клиент не отключится TODO - Сделать
	 * отключеие по таймеру
	 */
	private void createSession() {
		String userReply = null;
		// Этот код не красивый. и когда нибудь я возможно его исправлю.
		JSONObject parsedRequest = null;
		do {
			userReply = userChannel.readLine();
			parsedRequest = parseRequest(userReply);
			userChannel.clearDebugInfo();
			sendToClient(tryToDo(parsedRequest), userChannel.readDebug());

		} while (sessionAlive);
	}

	/*
	 * Отправить клиенту сообщение
	 */
	final protected void sendToClient(boolean result, String message) {
		JSONObject reply = new JSONObject();
		reply.put("message", message);
		reply.put("success", result);
		myUser.getChannel().println(reply.toString());
	}

	/*
	 * Отправить клиенту сообщение, в качестве параметра передаётся JSONObject
	 */
	final protected void sendToClient(boolean result, JSONObject message) {
		sendToClient(result,message.toString());
	}

	/*
	 * Парсинг и распознавание того что отправил клиент TODO реализовать защиту
	 * от переполнения памяти и других нехороших вещей.
	 */
	final protected JSONObject parseRequest(String message) {
		JSONTokener requestTokener = new JSONTokener(message);
		return new JSONObject(requestTokener);
	}

	public boolean tryToDo(JSONObject userDirective) {
		if (userDirective.keySet().size() != 1) {
			userChannel.setDebug("count of arguments is not equal 1");
			return false;
		} else if (userDirective.has("set")) {
			return myUser.doSet(userDirective.get("set"));
		} else if (userDirective.has("get")) {
			return myUser.doGet(userDirective.get("get"));
		} else if (userDirective.has("keepAlive")) {
			userChannel.setDebug("keepAlive received");
			return true;
		} else if (userDirective.has("logout")) {
			userChannel.setDebug("logout received, bye!");
			sessionAlive = false;
			return true;
		} else {
			userChannel.setDebug("Unknown action");
			return false;
		}
	}
}
