package general;

import java.util.ArrayList;

import functions.PwmWorker;
import network.ConnectionHandler;
import supporting.InputOutput;

public class InitServer {
	private static PwmWorker pwm = new PwmWorker();

	public static void main(String[] args) {

		// Здесь инициализация PWM
		System.out.println("Please type pin number to use PWM *1, 23, 24, 26) or use default parameters (type 0)"
				+ " < 0 (-1, -100) is end to adding");
		int pin;
		ArrayList<Integer> pins = new ArrayList<Integer>();
		do {
			pin = Integer.parseInt(System.console().readLine());
			if (pin > 0) {
				pins.add(pin);
				System.out.println("Pin added, now Array is: " + pins.toString() + "\nwrite new:");

			} else {
				break;
			}
		} while (true);

		// Если pin равен нулю значит пользователь использует дефолтные
		// настройки PWM.
		if (pin == 0) {
			pins.clear();
			pins.add(1);
		}

		System.out.println("Pins Array is: " + pins.toString());
		pwm.setPinForPwm(pins);

		// Далее будет инициализация компонентов.
		// Инициализация сетевого сервера:
		Thread server = new Thread(new ConnectionHandler(pwm));
		server.start();

		// Инициализация консольного диалога
		startDialogConsole(pwm);
		System.exit(0);

	}

	private static void startDialogConsole(PwmWorker pwm) {
		/*
		 * Здесь программа через консоль спрашивает пользователя какой режим
		 * установить. Пока что для локальных тестов. Позже через консоль будут
		 * задаваться другие настройки.
		 */
		// Консольное управление:
		InputOutput console = new InputOutput();
		InteractiveServer consoleDialog = new InteractiveServer(pwm, console);
		consoleDialog.getService("Это главное меню. Если закончить работу здесь то программа закроется.");
	}

}
