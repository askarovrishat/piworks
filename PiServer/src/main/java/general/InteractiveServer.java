package general;

import java.io.IOException;

import functions.PwmWorker;
import network.ProtocolManager;
import rishatik92.PiService.PwmKindOfWork;
import supporting.AdminUser;
import supporting.InputOutput;
import supporting.RaspberryUser;

public class InteractiveServer {
	/*
	 * Консольный ассистент. Реализует управление ШИМ контроллером через
	 * консоль. Создавался для возможности быстрых тестов. Работает безупречно,
	 * как топор.
	 */
	private PwmWorker pwm;
	private InputOutput channel;

	public InteractiveServer(PwmWorker workingPwm, InputOutput chan) {
		pwm = workingPwm;
		channel = chan;
	}

	private void consoleChooseTypeWork(RaspberryUser user) {
		/*
		 * Просим юзера выбрать режим работы ШИМ контроллера.
		 */
		int choice = 0;
		String scenario = null;
		while (true) {
			channel.println("type a choise, what need do:");
			scenario = "1: start blinking\n2: set volume\n3: nothing\n4: exit";
			channel.println(scenario);
			choice = Integer.parseInt(channel.readLine());
			switch (choice) {
			case 1:
				// start blink
				// ask change time
				channel.println("Change time for blinking [0-100], another - exit");
				int timeForBlinking = Integer.parseInt(channel.readLine());
				user.setPwmtoBlink(timeForBlinking);
				break;
			case 2:
				// stop blink
				// ask volume and set
				channel.println("Change volume [0-100], another - exit");
				int volume = Integer.parseInt(channel.readLine());
				user.setVolumePWM(volume);
				break;
			case 3:
				// stop blink
				user.turnOff();
				break;
			case 4:
				user.turnOff();
				return;
			default:
				channel.println("unknown choosen: " + choice);
			}
		}
	}

	public void getService(String promt) {
		/*
		 * Предоставить в зависимости от предоставленной информации нужный
		 * интерфейс. console, service или GET (для браузеров)
		 */
		String thankYou = ("coming soon....\n");
		String reply = "";
		try {
			try {
				// Внимание - тут небольшой фокус..
				// Ждём секунду, на то чтобы получить информацию от браузера.
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// смотрим, есть ли что нибудь в буфере. Если браузер - то он
			// пришлёт что нить.
			if (channel.ready()) {
				reply = channel.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (reply.startsWith("GET")) {
			channel.println(
					"HTTP/1.1 200 OK\nContent-Type: text/plain\nContent-Length: 13\nConnection: close\n\n" + thankYou);
			// Если это не браузер, тогда диалог с юзером..
		} else {
			channel.println(promt + "\n" + "Please select one of next working mode:");
			channel.println("console\nservice\n");
			reply = channel.readLine();
			AdminUser admin = new AdminUser("admin", channel);
			admin.setPwm(pwm);
			// if console
			if (reply.startsWith("c")) {
				consoleChooseTypeWork(admin);
			} else if (reply.startsWith("s")) {
				// if service..
				ProtocolManager serviceWorker = new ProtocolManager(admin);
			} else {
				channel.println("Sorry, don't understand you. Try again.. later ;-)");
			}
		}

	}
}
