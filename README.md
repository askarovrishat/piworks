# Сервер для raspberryPi на Java / Server for manage raspberryPi based on Java
Упрвление ШИМ через консоль или tcp соединение. / manage PWM through console or tcp-connection.

Теперь есть поддержка работы в качестве сервиса. Команды подаются в формате JSON. примеры команд: / now supplied woorking as service. Managing commands are json-like. examples of comands:

Включить плавное изменение яркости с временем 10 / flowing change brigth with time 10: (time between 0 and 100)
{"set":{"pwm":{"type":"blink","time":10}}}

Включить PWM с яркостью от 0 до 100 / enable PWM with brightness 100:
{"set":{"pwm":{"type":"set","volume":100}}}

Выключить PWM / disable PWM:
{"set":{"pwm":{"type":"turnOff"}}

Отключиться от сервиса: / logout from service
{"logout":true}